#!/bin/bash
#GOOS=js GOARCH=wasm go build -o main.wasm
export CI_PROJECT_ID="00000"
export CI_MERGE_REQUEST_IID="11111"
export CI_PROJECT_PATH="SOMEWHERE"
export CI_COMMIT_REF_SLUG="main"

GOOS=js GOARCH=wasm go build \
  -ldflags "-X main.ciProjectId=$CI_PROJECT_ID -X main.ciMergeRequestIid=$CI_MERGE_REQUEST_IID -X main.ciProjectPath=$CI_PROJECT_PATH -X main.ciCommitRefSlug=$CI_COMMIT_REF_SLUG" \
  -o index.wasm

ls -lh *.wasm

# This script is to build the wasm file en dev mode
# We don't need to publish this file to public
